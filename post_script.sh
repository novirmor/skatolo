#!/usr/bin/env sh

if [ ! -d "output" ]; then
    mkdir output
fi

if [ ! -d "logs" ]; then
    mkdir logs
fi

mv build.log logs/${1}-${2}.log

mv build/* output/

rm -rf build
