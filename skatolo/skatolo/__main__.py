from sys import version_info
from argparse import ArgumentParser

if version_info <= (3, 6):
    raise Exception("Must be using Python 3.6+")
else:
    from skatolo.generator import generator
    from skatolo.packer import packer


def build_command(subparsers):
    build = subparsers.add_parser("build")
    build.add_argument("-o", "--operating-system", type=str,
                       action='store', dest='os',  required=True,
                       help="which os build")
    build.add_argument("-b", "--builder", type=str, choices=["qemu", "vbox"],
                       action='store', dest='builder', required=True,
                       help="which builder use")


def validate_command(subparsers):
    validate = subparsers.add_parser("validate")
    validate.add_argument("-o", "--operating-system", type=str,
                          action='store', dest='os',  required=True,
                          help="which os build")
    validate.add_argument("-b", "--builder", type=str, choices=["qemu", "vbox"],
                          action='store', dest='builder', required=True,
                          help="which builder use")


def main():
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest='func')
    build_command(subparsers)
    validate_command(subparsers)
    args = parser.parse_args()

    if args.func == "build":
        json = generator(args.os, args.builder).generate_json()
        packer(args.builder, json).validate()
        packer(args.builder, json).build()
    if args.func == "validate":
        json = generator(args.os, args.builder).generate_json()
        packer(args.builder, json).validate()


if __name__ == '__main__':
    main()
