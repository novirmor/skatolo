from subprocess import run, PIPE
from os import environ


class packer:
    def __init__(self, builder, json):
        self.json = json
        if builder == 'vbox':
            self.builder = 'virtualbox-iso'
        if builder == 'vmware':
            self.builder = 'vmware-iso'
        if builder == 'qemu':
            self.builder = 'qemu'

    def validate(self):
        cmd = ['packer', 'validate',  f"-only={self.builder}", self.json]
        process = run(cmd, stdout=PIPE)
        if process.returncode != 0:
            raise Exception(
                f"Generated string is not valid\n packer msg:{process.stdout}")
        print("Generated json is valid")

    def build(self):
        cmd = ['packer', 'build', f"-only={self.builder}", self.json]
        env = dict()
        env["PACKER_LOG"] = "1"
        env["PACKER_LOG_PATH"] = "build.log"
        env.update(environ.copy())
        process = run(cmd, env=env)
