from json import load, dump
from os import path


class generator:
    def __init__(self, os, builder):
        if not path.isfile(f"default.json"):
            raise Exception(f"file: default.json not exist")
        if not path.isfile(f"distros/{os}/vars.json"):
            raise Exception(f"file: distros/{os}/vars.json not exist")
        if not path.isdir(f"distros/{os}/http/{builder}"):
            raise Exception(
                f"dir: distros/{os}/http/{builder} not exist")
        self.os = os
        if builder == 'vbox':
            self.builder = 'virtualbox-iso'
        if builder == 'vmware':
            self.builder = 'vmware-iso'
        if builder == 'qemu':
            self.builder = 'qemu'

    def generate_json(self):
        with open('default.json', 'r') as json_data:
            default = load(json_data)
        with open('template.json', 'r') as json_data:
            template = load(json_data)
        with open(f"distros/{self.os}/vars.json", 'r') as json_data:
            var = load(json_data)
        template["variables"] = default["variables"]
        template["variables"].update(var["variables"])
        for builder in template["builders"]:
            if builder["type"] == self.builder:
                builder["boot_command"] = var["boot_command"]
        with open("generated.json", 'w') as outfile:
            dump(template, outfile)
        return "generated.json"
