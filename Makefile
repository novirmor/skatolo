all: qemu vbox
clean:
	rm -rf *output *.log

qemu: alpine_qemu centos_qemu debian_qemu fedora_qemu ubuntu_qemu
vbox: alpine_vbox centos_vbox debian_vbox fedora_vbox ubuntu_vbox

alpine: alpine_vbox alpine_qemu
alpine_vbox:
	python -m skatolo build -o alpine -b vbox
alpine_qemu:
	python -m skatolo build -o alpine -b qemu

centos: centos_vbox centos_qemu
centos_vbox:
	python -m skatolo build -o centos -b vbox
centos_qemu:
	python -m skatolo build -o centos -b qemu

debian: debian_vbox debian_qemu
debian_vbox:
	python -m skatolo build -o debian  -b vbox
debian_qemu:
	python -m skatolo build -o debian -b qemu

ubuntu: ubuntu_vbox ubuntu_qemu
ubuntu_vbox:
	python -m skatolo build -o ubuntu  -b vbox
ubuntu_qemu:
	python -m skatolo build -o ubuntu -b qemu

fedora: fedora_vbox fedora_qemu
fedora_vbox:
	python -m skatolo build -o fedora  -b vbox
fedora_qemu:
	python -m skatolo build -o fedora -b qemu
